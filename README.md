# "My First API" Project

## About

College project, based on [these directives](https://gitlab.com/unimar/myfirstapi).  
You follow what is done here by using the [commit history](#commits) or the [step-by-step](#step-by-step) provided.  

## Step by Step

- For this to work, you will need to have installed [PHP 8 or newer](https://www.php.net/), [Composer](https://getcomposer.org/), and **MySQL Server**, here used on [Docker containers](https://www.docker.com/)
- Run command to **initialize the project**:
```
composer create-project laravel/laravel --prefer-dist ProductsApi
```
- Create the Docker configuration file (**compose.yaml**) [Attention as you might have to change the ports]
- Run command to **setup the container**:
```
docker compose up -d
```
![Docker all set up](screens/commit_03_01.png)
- Check **localhost** to confirm it is working
![PHPMyAdmin and MySQLServer working on localhost](screens/commit_03_02.png)
- Run Laravel commands to **create tables**:
```
php artisan make:migration create_products_table
php artisan make:migration create_customers_table
php artisan make:migration create_transactions_table
```
- **Setup table** on newly created migration files
- You will have to create the database before creating the tables with the Laravel command to **setup the database**:
```
php artisan migrate
```
- Run commands to **create controllers and models**:
```
php artisan make:controller Api\\ProductController --model=Product --api
php artisan make:controller Api\\CustomerController --model=Customer --api
php artisan make:controller Api\\TransactionController --model=Transaction --api
```
- Update newly added files with information for CRUD operation
- Create routes for **endpoints in file routes/api.php**
- Check endpoints, serve API and test it, with commands
```
php artisan route:list
php artisan serve
```

This will already make it possible to make CRUD operations with listed endpoints.  
The following commits will be fluff.

## How the API Works

There are three tables: Customers, Products, and Transactions.  
Customers have the "name" (STR) attribute and are given an "id" attribute.  
Products have the "description" (STR) and "stock" (INT) attributes and are given an "id" attribute.  
Transactions have the "datetime" (DATETIME), "customer_id" (INT) and "transaction" (STR) attributes and are given an "id attribute".

All returns will have their creation and last update times, if available.

**Customers**
GET: returned data:
```
{
    {
        "id": 1,
        "name": "First Customer",
        "created": "[date of creation]",
        "updated": "[date of last update]"
    },
    {
        "id": 2,
        "name": "Second Customer",
        "created": "[date of creation]",
        "updated": "[date of last update]"
    },
    ...
}
```
GET {id}: returned specific data:
```
Case not found: page with error 404

Case found:
{
    "message": "Customer found",
    "customer":
    {
        "id": {id},
        "name": "Specific Customer",
        "created": "[date of creation]",
        "updated": "[date of last update]"
    }
}
```
POST: expected data:
```
Case invalid entry:
{
    "message": "Your request is missing data",
    "errors": [
        "'name' field must be at least 1 character long"
    ]
}

Case valid entry:
{
    "message": "Customer created successfully",
    "customer": {
        "id": {id},
        "name": "New Customer",
        "created": "[date of creation]"
    }
}
```
PUT {id}: expected data:
```
Case not found: page with error 404

Case found AND invalid entry:
{
    "message": "Your request is missing data",
    "errors": [
        "'name' field must be at least 1 character long"
    ]
}

Case found AND valid entry:
{
    "message": "Customer updated",
    "customer":
    {
        "id": {id},
        "name": "Specific Customer",
        "created": "[date of creation]",
        "updated": "[date of last update]"
    }
}
```
DELETE {id}: returned data:
```
Case not found: page with error 404

Case found:
{
    "message": "Customer deleted"
}
```

**Products**
GET: returned data:
```
{
    {
        "id": 1,
        "description": "First Product",
        "stock": [quantity in stock],
        "created": "[date of creation]",
        "updated": "[date of last update]"
    },
    {
        "id": 2,
        "description": "Second Product",
        "stock": [quantity in stock],
        "created": "[date of creation]",
        "updated": "[date of last update]"
    },
    ...
}
```
GET {id}: returned specific data:
```
Case not found: page with error 404

Case found:
{
    "message": "Product found",
    "product":
    {
        "id": {id},
        "description": "Specific Product",
        "stock": [quantity in stock],
        "created": "[date of creation]",
        "updated": "[date of last update]"
    }
}
```
POST: expected data:
```
Case invalid entry:
{
    "message": "Your request is missing data",
    "errors": [
        "'description' field must be at least 1 character long",
        "'stock' field must be an integer"
    ]
}

Case valid entry:
{
    "message": "Product created successfully",
    "customer": {
        "id": {id},
        "description": "New Product",
        "stock": [quantity in stock],
        "created": "[date of creation]"
    }
}
```
PUT {id}: expected data:
```
Case not found: page with error 404

Case found AND invalid entry:
{
    "message": "Your request is missing data",
    "errors": [
        "'description' field must be at least 1 character long",
        "'stock' field must be an integer"
    ]
}

Case found AND valid entry WITH SAME data:
{
    "message": "Product was not updated",
    "errors": "Data received is the same as data stored"
}

Case found AND valid entry WITH DIFFERENT data:
{
    "message": "Product updated",
    "product": {
        "id": {id},
        "description": "Specific Product",
        "stock": [quantity in stock],
        "created": "[date of creation]",
        "updated": "[date of last update]"
    }
}
```
DELETE {id}: returned data:
```
Case not found: page with error 404

Case found:
{
    "message": "Product deleted"
}
```

**Transactions**
GET: returned data:
```
{
    {
        "id": 1,
        "customer_id": [customer_id],
        "transaction": {
            "product": [relevant product],
            "quantity": [quantity]
        },
        "created": "[date of transaction]"
    },
    {
        "id": 2,
        "customer_id": [customer_id],
        "transaction": {
            "product": [relevant product],
            "quantity": [quantity]
        },
        "created": "2022-10-04 14:42:12 UTC"
    },
    ...
}
```
GET {id}: returned specific data:
```
{
    "message": "Transaction found",
    "transaction": {
        "id": {id},
        "customer_id": [customer_id],
        "stock": {
            "product": [relevant product],
            "quantity": [quantity]
        },
        "created": "[date of creation]"
    }
}
```
POST: expected data:
```
Expected data received:
{
    "customer_id": [integer],
    "transaction": {
        "product": [integer],
        "quantity": [integer]
    }
}

Case invalid "customer_id":
{
    "message" => "Your request is missing data",
    "errors" => "Given 'customer_id' does not exists in the database"
}

Case invalid "transaction":
{
    "message" => "Your request is missing data",
    "errors" => "'transaction' must be formatted as instructed in documentation"
}
or
{
    "message" => "Your request is missing data",
    "errors" => "Given 'product' does not exists in the database"
}
or
{
    "message" => "Your request is missing data",
    "errors" => "Given 'product' does not have enough stock"
}

Case valid:
{
    "message": "Transaction computed",
    "customer_id": [customer_id],
    "transaction": {
        "product": [product_id],
        "quantity": [quantity]
    }
}
```

## Commits

- commit_01: initial GitLab setup
- commit_02: creating the project
- commit_03: setting up connections
- commit_04: creating tables
- commit_05: create models and controllers
- commit_06: create endpoints and test
- commit_07: FLUFF!
