<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();

        $return = [];
        $i = 0;

        foreach ($transactions as $entry) {
            $index = new Product();
            
            $index->id = $entry->id;
            $index->customer_id = $entry->customer_id;
            $index->transaction = json_decode($entry->transaction);
            $index->created = $entry->created_at->format('Y-m-d H:i:s T');

            $return[$i] = $index;
            $i++;
        }

        return response()->json($return);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check for customer id
        $targetCustomer = $request->customer_id;

        $customers = Customer::all();

        $isValid = false;

        foreach ($customers as $entry) {
            if ($entry->id == $targetCustomer) {
                $isValid = true;
            }
        }

        if ($isValid == false) {
            return response()->json([
                'message' => 'Your request is missing data',
                'errors' => "Given 'customer_id' does not exists in the database"
            ], 400);
        }
        
        // get transaction
        $validated = Validator::make($request->all(), [
            'customer_id' => 'required',
            'transaction' => 'required'
        ]);

        $tran = json_encode($request->transaction);
        $tran = json_decode($tran);

        $tranProduct = $tran->product;
        $tranQuantity = $tran->quantity;

        if ($tranProduct == null || $tranQuantity == null || gettype($tranProduct) != 'integer' || gettype($tranQuantity) != 'integer') {
            return response()->json([
                'message' => 'Your request is missing data',
                'errors' => "'transaction' must be formatted as instructed in documentation"
            ], 400);
        }

        // check for product and stock
        $products = Product::all();

        $isValid = false;
        $hasStock = true;
        $targetProduct = new Product();

        foreach ($products as $entry) {
            if ($entry->id == $tranProduct && $entry->stock >= $tranQuantity) {
                $isValid = true;
                $targetProduct = $entry;
            }

            if ($entry->id == $tranProduct && $entry->stock < $tranQuantity) {
                $isValid = true;
                $hasStock = false;
            }
        }

        if ($isValid == false) {
            return response()->json([
                'message' => 'Your request is missing data',
                'errors' => "Given 'product' does not exists in the database"
            ], 400);
        }

        if ($hasStock == false) {
            return response()->json([
                'message' => 'Your request is missing data',
                'errors' => "Given 'product' does not have enough stock"
            ], 400);
        }

        // remove from stock and post
        $updateRequest = new Request();

        $targetProduct->stock = $targetProduct->stock - $tranQuantity;

        app(ProductController::class)->update($updateRequest, $targetProduct);


        Transaction::create([
            'datetime' => date_create(),
            'customer_id' => $targetCustomer,
            'transaction' => json_encode($request->transaction)
        ]);

        return response()->json([
            'message' => 'Transaction computed',
            'customer_id' => $targetCustomer,
            'transaction' => $request->transaction
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return response()->json([
            'message' => 'Transaction found',
            'transaction' => [
                'id' => $transaction->id,
                'customer_id' => $transaction->customer_id,
                'stock' => json_decode($transaction->transaction),
                'created' => $transaction->created_at->format('Y-m-d H:i:s T')
            ]
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update() //Request $request, Transaction $transaction)
    {
        return response()->json([
            'message' => 'Update operations not allowed for transactions'
        ], 405);

        /*
        $validated = Validator::make($request->all(), [
            'datetime' => 'min:1',
            'transaction' => 'min:1'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
        };

        $transaction->update($request->all());

        return response()->json([
            'message' => 'Transaction updated',
            'transaction' => $transaction
        ], 200);
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy() //Transaction $transaction)
    {
        return response()->json([
            'message' => 'Delete operations not allowed for transactions'
        ], 405);

        /*
        $transaction->delete();

        return response()->json([
            'message' => 'Transaction deleted'
        ], 200);
        */
    }
}
