<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        $return = [];
        $i = 0;

        foreach ($products as $entry) {
            $index = new Product();
            
            $index->id = $entry->id;
            $index->description = $entry->description;
            $index->stock = $entry->stock;
            $index->created = $entry->created_at->format('Y-m-d H:i:s T');
            $index->updated = ($entry->updated_at != null) ? $entry->updated_at->format('Y-m-d H:i:s T') : null;

            $return[$i] = $index;
            $i++;
        }

        return response()->json($return);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedDescription = Validator::make($request->all(), [
            'description' => 'required|min:1'
        ]);
        $validatedStock = Validator::make($request->all(), [
            'stock' => 'required|numeric|integer'
        ]);

        $errors = [];
        if ($validatedDescription->fails())
            array_push($errors, "'description' field must be at least 1 character long");
        if ($validatedStock->fails())
            array_push($errors, "'stock' field must be an integer");

        $response = response()->json([
            'message' => 'Your request is missing data',
            'errors' => $errors
        ], 400);
        if ($validatedDescription->fails() || $validatedStock->fails())
            return $response;

        $product = Product::create($request->all());

        return response()->json([
            'message' => 'Product created successfully',
            'product' => [
                'id' => $product->id,
                'description' => $product->description,
                'stock' => $product->stock,
                'created' => $product->created_at->format('Y-m-d H:i:s T')
            ]
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return response()->json([
            'message' => 'Product found',
            'product' => [
                'id' => $product->id,
                'description' => $product->description,
                'stock' => $product->stock,
                'created' => $product->created_at->format('Y-m-d H:i:s T'),
                'updated' => ($product->updated_at != $product->created_at) ? $product->updated_at->format('Y-m-d H:i:s T') : null                
            ]
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validatedDescription = Validator::make($request->all(), [
            'description' => 'sometimes|required|min:1'
        ]);
        $validatedStock = Validator::make($request->all(), [
            'stock' => 'sometimes|required|numeric|integer'
        ]);

        $errors = [];
        if ($validatedDescription->fails())
            array_push($errors, "'description' field must be at least 1 character long");
        if ($validatedStock->fails())
            array_push($errors, "'stock' field must be an integer");

        $response = response()->json([
            'message' => 'Your request is missing data',
            'errors' => $errors
        ], 400);
        if ($validatedDescription->fails() || $validatedStock->fails()) {
            return $response;
        };

        if (($request->description == $product->description) && ($request->stock == $product->stock)) {
            return response()->json([
                'message' => 'Product was not updated',
                'errors' => 'Data received is the same as data stored'
            ], 200);
        }

        $product->update($request->all());

        return response()->json([
            'message' => 'Product updated',
            'product' => [
                'id' => $product->id,
                'description' => $product->description,
                'stock' => $product->stock,
                'created' => $product->created_at->format('Y-m-d H:i:s T'),
                'updated' => ($product->updated_at != $product->created_at) ? $product->updated_at->format('Y-m-d H:i:s T') : null
            ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json([
            'message' => 'Product deleted'
        ], 200);
    }
}
