<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();

        $return = [];
        $i = 0;

        foreach ($customers as $entry) {
            $index = new Customer();
            
            $index->id = $entry->id;
            $index->name = $entry->name;
            $index->created = $entry->created_at->format('Y-m-d H:i:s T');
            $index->updated = ($entry->updated_at != null) ? $entry->updated_at->format('Y-m-d H:i:s T') : null;

            $return[$i] = $index;
            $i++;
        }

        return response()->json($return);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|min:1'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data',
                'errors' => [
                    "'name' field must be at least 1 character long"
                ]
            ], 400);
        }

        $customer = Customer::create($request->all());

        return response()->json([
            'message' => 'Customer created successfully',
            'customer' => [
                'id' => $customer->id,
                'name' => $customer->name,
                'created' => $customer->created_at->format('Y-m-d H:i:s T')
            ]
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {       
        return response()->json([
            'message' => 'Customer found',
            'customer' => [
                'id' => $customer->id,
                'name' => $customer->name,
                'created' => $customer->created_at->format('Y-m-d H:i:s T'),
                'updated' => ($customer->updated_at != $customer->created_at) ? $customer->updated_at->format('Y-m-d H:i:s T') : null
            ]
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|min:1'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data',
                'errors' => [
                    "'name' field must be at least 1 character long"
                ]
            ], 400);
        };

        $customer->update($request->all());

        return response()->json([
            'message' => 'Customer updated',
            'customer' => [
                'id' => $customer->id,
                'name' => $customer->name,
                'created' => $customer->created_at->format('Y-m-d H:i:s T'),
                'updated' => ($customer->updated_at != $customer->created_at) ? $customer->updated_at->format('Y-m-d H:i:s T') : null
            ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        return response()->json([
            'message' => 'Customer deleted'
        ], 200);
    }
}
